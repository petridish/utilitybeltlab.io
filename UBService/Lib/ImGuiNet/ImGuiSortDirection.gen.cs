#pragma warning disable 1591
namespace ImGuiNET
{
    public enum ImGuiSortDirection
    {
        None = 0,
        Ascending = 1,
        Descending = 2,
    }
}
